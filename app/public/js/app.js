function readStartOfFile(file, start, end) {
    return new Promise(
        (resolve, reject) => {
            const reader = new FileReader();
            reader.onload = () => {
                resolve(reader.result);
            };
            reader.onerror = reject;
            reader.readAsArrayBuffer(file.slice(start, end));
        }
    );
}

function checkMagicArray(bytes, magic) {
    for (var i = 0 ; i < magic.length ; i++) {
        if (bytes[i] != magic[i])
            return false;
    }
    return true;
}

function checkMagic(bytes, magic_object) {
    for (const [type, magic] of Object.entries(magic_object)) {
        if (checkMagicArray(bytes, magic))
            return type;
    }
    return null;
}

function checkAlias(file, alias) {
    const ext = file.name.split('.').pop();
    if (ext in alias)
        return alias[ext];
    else
        return null;
}

function setSelections(type) {
    if (type !== null) {
        const inputOpt = document.querySelector(`option#input_${type}`);
        if (inputOpt !== null)
            inputOpt.selected = true;
        const outputOpt = document.querySelector(`option#output_${type}`);
        if (outputOpt !== null)
            outputOpt.selected = false;
    }
}

document.addEventListener(
    'DOMContentLoaded',
    function(){
        const alias = JSON.parse(
            document.querySelector('#alias-json').textContent
        );
        const magic_dict = JSON.parse(
            document.querySelector('#magic-json').textContent
        );
        const magic_len = magic_dict['max_len'];
        const magic = magic_dict['magic'];
        document.querySelector("input#upload").addEventListener(
            'change',
            (event) => {
                const uploads = event.target.files;
                const upload = uploads[0];
                readStartOfFile(upload, 0, magic_len)
                    .then(
                        (blob) => {
                            const bytes = new Uint8Array(blob);
                            var type =
                                checkMagic(bytes, magic) ||
                                checkAlias(upload, alias);
                            setSelections(type);
                        }
                    );
            }
        );
        document.querySelector('p#noscript').style.display = 'none';
    },
    false
);
