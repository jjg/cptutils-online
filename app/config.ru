# -*- ruby -*-

require 'rack'
require './app'

mime = {}
[
  {
    exts: ['cpt', 'c3g', 'ggr', 'gpf', 'inc', 'svg', 'sao', 'map'],
    mime: 'text/x-cmap-cpt'
  },
  {
    exts: ['lut', 'grd', 'png', 'PspGradient'],
    mime: 'application/octet-stream'
  }
].each do |h|
  h[:exts].each do |ext|
    mime['.' + ext] = h[:mime]
  end
end

Rack::Mime::MIME_TYPES.merge!(mime)

use CptUtils

CptUtils.set(:config, 'config.yaml')
run CptUtils
