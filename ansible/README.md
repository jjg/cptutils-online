Ansible
-------

This a work in progress.

There are several Makefile targets:

- `apt-upgrade-production`, performs an `apt-get` update/upgrade on
  the target.

- `cptutils-production`, installs **cptutils** on production.

- `app-production`, installs the application on production.  There
  is some manual configuration needed: the code below needs to be
  added to the target virtual host.

```
PassengerRuby /usr/bin/ruby
PassengerUserSwitching on
PassengerUser www-data
PassengerGroup www-data

Alias /pub/cptutils-online /usr/local/share/cptutils-online/public
<Location /pub/cptutils-online>
        PassengerBaseURI /pub/cptutils-online
        PassengerAppRoot /usr/local/share/cptutils-online
</Location>
<Directory /usr/local/share/cptutils-online/public>
        Allow from all
        Options -MultiViews
        Require all granted
</Directory>
```
