#!/bin/sh

set -e

# experimental plugin for munin, this just does a file-count in
# the results directory, since we have a cron-job to clear out
# files older than a week, this gives a nice outline metric on
# application usage.  This should be moved into ansible at some
# point (but only installed if munin is present, so check that
# /usr/share/munin/plugins exists first ...

case $1 in
   config)
        cat <<EOF
graph_title cptutils-online
graph_info The number of gradients converted in the previous week
graph_args --lower-limit 0
graph_vlabel converted
converted.label converted
EOF
        exit 0;;
esac

# this could be install-time configurable

RESULTS='/var/lib/cptutils-online/results'

if [ -d "${RESULTS}" ]
then
    count=$(ls -1 "${RESULTS}" | wc -l)
else
    count=0
fi

echo "converted.value ${count}"
